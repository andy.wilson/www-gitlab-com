---
layout: markdown_page
title: "GitLab POC Template"
---


## Guided Proof of Concept (POC) Guidelines

### Proof of Concept (POC) Template

GitLab wants prospects to enjoy a successful Proof of Concept with GitLab Enterprise Edition. In the POC [document](https://docs.google.com/document/d/1anbNik_H_XDHJYyZPkfr4_6wvIXgqbwvWynN9v9tj2E/edit#) (only accessible to GitLab team members and candidates), provides the framework for a successful POC by addressing current state issues, persistent challenges, business problems, desired state and outcomes.  
  
This document suggests and verifies specific success criteria for any POC, as well as outlining a mutual commitment between GitLab and the identified prospect parties. It also specifies the limited timeframe in which the POC will occur.  
  
### Using the Proof of Concept (POC) Template

The template provides a standardized approach to the POC process, but the document requires customization for each and every POC to be executed.  
  
To use the template, begin by making a copy of the master document for each POC.  
  
Edit each area highlighted in yellow within the document to include pertinent information for any particular prospect. This information includes basic data like the prospect name and GitLab team member details, as well as data to be collaboratively identified with the prospect, such as success criteria, trial location, and the client pilot team(s).  
  
After all the highlighted sections have been completely filled in, collaboratively complete the **Date** and **Owner** column fields within the **Project Plan** and **Roles and Responsibilities** sections.  
  
Finally, ensure both GitLab and the prospect have a copy of the document. Schedule all meetings associated to the POC via calendar invites prior to distributing the GitLab Enterprise Edition license for the POC.  
