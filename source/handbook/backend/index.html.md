---
layout: markdown_page
title: "Backend Group"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Teams

There are a number of teams within the Backend group:

* [CI/CD](/handbook/backend#cicd)
* [Discussion](/handbook/backend#discussion)
* [Platform](/handbook/backend#platform)
* [Monitoring](/handbook/backend#monitoring)

Each team has a different focus on what issues to work on for each
release. The following information is not meant to be a set of hard-and-fast
rules, but as a guideline as to what team decides can best improve certain
areas of GitLab.

APIs should be shared responsibility between all teams within the
Backend group.

There is a backend group call every Tuesday, before the team call. You should
have been invited when you joined; if not, ask your team lead!

[Find the product manager mapping to engineering teams in the product handbook](/handbook/product)

### CI/CD Team
{: #cicd}

The CI/CD Team is focused on all the functionality with respect to
Continuous Integration and Deployments.

This team maps to [Verify, Package, Release, and Configure](https://about.gitlab.com/handbook/product/categories/#ops).

### Discussion Team
{: #discussion}

The Discussion Team is focused on the collaboration functionality of GitLab.

This team maps to [Plan and Create](https://about.gitlab.com/handbook/product/categories/#dev).

### Platform Team
{: #platform}

The Platform Team is focused on all the other areas of GitLab that
the CI and Discussion Teams do not cover.

This team maps to [Create and Auth](https://about.gitlab.com/handbook/product/categories/#dev).

### Monitoring Team
{: #monitoring}

[Monitoring](https://about.gitlab.com/solutions/monitor/) is focused on providing solutions to monitor both GitLab itself as well as customer applications.

This team maps to [Monitor](https://about.gitlab.com/handbook/product/categories/#ops).
