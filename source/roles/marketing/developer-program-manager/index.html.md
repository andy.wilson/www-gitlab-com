---
layout: job_page
title: "Developer Program Manager"
---

As the Developer Program Manager, you will be responsible for serving as advocate and evangelist working to grow, cultivate and support the GitLab community of developers and all those passionate about GitLab. You will help to make it easy to contribute to GitLab, help our community give feedback to our product and documentation, and help to create educational programs for GitLab.

## Responsibilities

* Develop and manage developer engagement programs for external communities to gather feedback, engage in dialogue, educate and evangelize GitLab. Results are mesaured based on growing the number of Developers using GitLab. Key results include quarterly community growth targets and targets for increase in engagement.
* Manage sponsorships, participate in events and community meetups, create new meetups, host webinars and organize talks. Results are measured for the number of people engaged with for each activity, and the number of conversions from each activity. 
* Organize GitLab activities to include meetups, talks and workshops. You are accountable for how many people are touched by these GitLab events, with results measured based on number of activites and number of people engaged.
* Channel your passions into the developer experience of the GitLab community, providing the very best support, guidance and engagement to empower developers using GitLab.
* Partner with the technical writing team and content marketing to plan and help create engaging developer content for our blog, video, social media, and other outlets.
* Collaborate with our product and technical writing teams to define and launch scalable tutorials and quickstart guides to support developers onboarding and adoption of GitLab.
* Work with our product marketing team to support our monthly release with developer education tools for the new features and products launching.
* Focus on developer experience and how developer relations can help get user feedback into the product and documentation.
* Support our corporate and field marketing events efforts through developer advocacy.
* Collect metrics and utilize findings to guide strategy across outreach efforts including meetups, community meetings, and events around the world.

## Requirements

* You have 5-7 years of experience running developer relations or community advocacy programs, preferably open source in nature.
* You are creative. You’ve made people happy with your quirky campaigns.
* You give a great keynote and write a great blog and have videos and articles to prove it.
* You have experience facilitating sensitive and complex community situations with humility, empathy, judgment, tact, and humor.
* Excellent spoken and written English.
* Technical background or good understanding of developer products; familiarity with Git, Continuous Integration, Containers, and Kubernetes a plus.
* Analytical and data driven in your approach to building and nurturing communities.
* You are obsessed with making developers happy. You know that the slightest trouble in getting started with a product can ruin customer happiness.
* Bonus points for having an existing network from a diverse set of communities and social media platforms.
* Experience in communicating with bloggers and other media on a range of technical topics is a plus.
* You share our values, and work in accordance with those values.
